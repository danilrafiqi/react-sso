import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import firebase from 'firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import Recaptcha from 'react-recaptcha';

firebase.initializeApp({
  apiKey: 'AIzaSyAccg9vML_-tPGbdDLrYiSp44H0A1zSB8E',
  authDomain: 'ssotugas.firebaseapp.com'
});

class App extends Component {
  state = {
    isSignedIn: false,
    isVerified: false,
    isClicked: false
  };
  uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      // firebase.auth.GithubAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccess: () => false
    }
  };

  reCaptchaLoaded = () => {
    console.log('captcha di load');
  };

  verifyCallback = res => {
    if (res) {
      this.setState({
        isVerified: true
      });
    }
  };

  loginBtn = () => {
    this.setState({
      isClicked: true
    });
  };
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      this.setState({
        isSignedIn: !!user
      });
    });
  }

  render() {
    return (
      <div className="App">
        {this.state.isSignedIn ? (
          <div>
            <h1>Welcome {firebase.auth().currentUser.displayName}</h1>
            <button onClick={() => firebase.auth().signOut()}>Sign out</button>
          </div>
        ) : this.state.isVerified ? (
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
        ) : (
          <div>
            {this.state.isClicked ? (
              <Recaptcha
                sitekey="6Le_XH0UAAAAAAIxyLwCn2q85hoI_orGfzclWZbp"
                render="explicit"
                onloadCallback={this.reCaptchaLoaded}
                verifyCallback={this.verifyCallback}
              />
            ) : (
              <button onClick={this.loginBtn}>Login</button>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default App;
